# On this page...

You can find some of my best projects made till now

### Prerequisites

Minimum: OpenJDK 8
Recommended: Oracle JDK 12 or OpenJDK 12

## Running programs
### For .java files
Just download the source files and compile them by using the commandline ( javac file && java output ), or by opening it in an
IDE, such as InteliJ IDEA or Eclipse
### For .cpp files
You'll need a c++ compiler, such as G++ or MSVC
### For .css, .js and .html files
Just run them in your browser
### .php files
You'll need to have at least a php interpreter or a php server (like [Wamp Server](http://www.wampserver.com), for example) , I recommend using 
[Laragon](https://laragon.org/) because it is very easy to deploy a website and is bundled with many cool features

## Built With
### Java files
* OpenJDK 8
* [Eclipse IDE](https://www.eclipse.org/downloads/packages/) and [InteliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows)
### HTML, CSS, JS and Php files
* [Visual Studio Code](https://code.visualstudio.com/download) and [Sublime Text](https://www.sublimetext.com/3)
### C++ programs 
* [Visual Studio](https://visualstudio.microsoft.com/)


## Authors

* **Trifan Grigore-Gabriel** 


## License

This project is licensed under the MIT License
