
public class VariableExample {
	public static void main(String[] args) {
		int X = 32;
		System.out.println("X has the value "+X);
		X = 21; // Now I don't need to declare X anymore when I change its values :D 
		System.out.println(X); // Saw?? I changed the X's value
		String Language = "Java";
		int version = 12;
		String JavaVersion= Language + " " + version; //Added " " so it can output Java 12
		// It could be the same if I would write Language + " " + 12
		
		
	}
}
